import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Row, Col} from 'react-bootstrap';
// import CanvasJSReact from './canvasjs.react';
// import { CanvasJS } from './canvasjs.react';
import { CanvasJSChart} from './canvasjs.react';
import Sidebar from './components/Sidebar';
import Topbar from './components/Topbar';
import fastfoodLogo from './img/fastfood-logo.png';
import coupon from './img/coupon.png';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sqSize: 180,
      percentage: 30,
      strokeWidth: 5,
      options : {
        data: [{				
          type: "spline",
          dataPoints: [
            { label: "SUN",  y: 12  },
            { label: "MON", y: 15  },
            { label: "TUE", y: 22  },
            { label: "WED",  y: 40  },
            { label: "THU",  y: 17  },
            { label: "FRI",  y: 46  }
          ]
        },
        {				
          type: "spline",
          dataPoints: [
            { label: "SUN",  y: 15  },
            { label: "MON", y: 12  },
            { label: "TUE", y: 19  },
            { label: "WED",  y: 9  },
            { label: "THU",  y: 24  },
            { label: "FRI",  y: 44  }
          ]
        },
        ]
      }
    };
  };

  componentDidMount() {
    
  }

	render() {
    // Size of the enclosing square
    const sqSize = this.state.sqSize;
    // SVG centers the stroke width on the radius, subtract out so circle fits in square
    const radius = (this.state.sqSize - this.state.strokeWidth) / 2;
    // Enclose cicle in a circumscribing square
    const viewBox = `0 0 ${sqSize} ${sqSize}`;
    // Arc length at 100% coverage is the circle circumference
    const dashArray = radius * Math.PI * 2;
    // Scale 100% coverage overlay with the actual percent
    const dashOffset = dashArray - dashArray * 30 / 100;

		return(
		  <div>
        <Sidebar active="dashboard">
        </Sidebar>
        
        <Topbar>
        </Topbar>

        <div className="content">
          <div className="smallHeader">General</div>
          <div className="bigHeader">Dashboard</div>
          <Row>
            <Col>
              <div className="header">SHOP INFO</div>
              <div className="shop-info">
                <div className="canvas-logo">
                  <img alt="Shop Logo" src={fastfoodLogo} />
                </div>
                <div className="company-info text-12 justify-content-center d-flex flex-direction-column">
                  <div className="company-name-rating pb-2">
                    <div className="company-name">Burger King</div>
                    <div>
                      <span className="fa fa-star checked" />
                      <span className="fa fa-star checked" />
                      <span className="fa fa-star checked" />
                      <span className="fa fa-star checked" />
                      <span className="fa fa-star" />
                    </div>
                  </div>
                  <div className="d-flex text-center mt-2 company-numbers">
                    <div className="text-16 font-weight-bold mr-2">
                      2450
                      <div className="text-12 font-weight-normal mt-minus-5">
                        Participants
                      </div>
                    </div>
                    <div className="text-16 font-weight-bold ml-2">
                      5
                      <div className="text-12 font-weight-normal mt-minus-5">
                        Campaigns
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col>
              <div className="header">TRAFFIC REPORT</div>
              <div className="chart-timestamp d-flex align-items-center justify-content-center">
                <div className="mr-3 c-pointer active">7D</div>
                <div className="mr-3 c-pointer">14D</div>
                <div className="mr-3 c-pointer">1M</div>
                <div className="mr-3 c-pointer">3M</div>
                <div className="mr-3 c-pointer">6M</div>
                <div>1Y</div>
              </div>
              <CanvasJSChart options = {this.state.options}
                />
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="header">DATA INSIGHT</div>
              <Row>
                <Col className="mw-fit d-flex align-items-center">
                  <div className="percentage-container">
                  <svg
                    width={this.state.sqSize}
                    height={this.state.sqSize}
                    viewBox={viewBox}>
                    <circle
                      className="circle-background"
                      cx={this.state.sqSize / 2}
                      cy={this.state.sqSize / 2}
                      r={radius}
                      strokeWidth={`${this.state.strokeWidth}px`} />
                    <circle
                      className="circle-progress"
                      cx={this.state.sqSize / 2}
                      cy={this.state.sqSize / 2}
                      r={radius}
                      strokeWidth={`${this.state.strokeWidth}px`}
                      // Start progress marker at 12 O'Clock
                      transform={`rotate(-90 ${this.state.sqSize / 2} ${this.state.sqSize / 2})`}
                      style={{
                        strokeDasharray: dashArray,
                        strokeDashoffset: dashOffset
                      }} />
                    </svg>
                    <div className="inner-circle"></div>
                    <div className="percentage-info">
                      <div className="prec">{this.state.percentage}%</div>
                      <div className="text-12">Conversation Rate</div>
                    </div>
                  </div>
                </Col>
                <Col>
                  <div className="insight-container mb-3">
                    <i className="far fa-eye" />
                    <div className="subheader">Impression</div>
                    <div className="header">5,230</div>
                  </div>
                  <div className="insight-container">
                    <i className="fas fa-filter filter-icon" />
                    <div className="subheader">Leads Generated</div>
                    <div className="header">1,569</div>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col>
              <div className="header">COUPONS</div>
              <Row className="mb-3">
                <Col sm className="mw-fit d-flex align-items-center">
                  <img alt="Coupon Icon" className="img-coupon" src={coupon} />
                </Col>
                <Col sm>
                  <div className="font-weight-bold text-16">10% OFF</div>
                  <div className="text-12">Unlimited</div>
                </Col>
                <Col sm className="mw-fit text-center">
                  <div className="font-weight-bold text-14 text-tosca">469</div>
                  <div className="text-12">Redeemed</div>
                </Col>
              </Row>
              <Row className="mb-3">
                <Col sm className="mw-fit d-flex align-items-center">
                  <img alt="Coupon Icon" className="img-coupon" src={coupon} />
                </Col>
                <Col sm>
                  <div className="font-weight-bold text-16">FREE CHEESEBURGER</div>
                  <div className="text-12">1000 Only</div>
                </Col>
                <Col sm className="mw-fit text-center">
                  <div className="font-weight-bold text-14 text-tosca">245</div>
                  <div className="text-12">Redeemed</div>
                </Col>
              </Row>
              <Row>
                <Col sm className="mw-fit d-flex align-items-center">
                  <img alt="Coupon Icon" className="img-coupon" src={coupon} />
                </Col>
                <Col sm>
                  <div className="font-weight-bold text-16">25% OFF</div>
                  <div className="text-12">Unlimited</div>
                </Col>
                <Col sm className="mw-fit text-center">
                  <div className="font-weight-bold text-14 text-tosca">1,246</div>
                  <div className="text-12">Redeemed</div>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
		);
	}
}

export default App;