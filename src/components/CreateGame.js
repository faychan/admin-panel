import React, { Component } from 'react';
import { Row, Col, Button, Image} from 'react-bootstrap';
import Sidebar from './Sidebar';
import Topbar from './Topbar';
import reactCSS from 'reactcss';
import { SketchPicker } from 'react-color';
import surveyQuizImage from '../img/plane.jpeg';
import clawMachineImage from '../img/claw.jpeg';

export class CreateGame extends Component {
	state = {
		gameType: "",
		rows: [{name:"", image: ""}, {name:"", image: ""}, {name:"", image: ""}],
		displayColorPicker: false,
    color: {
      r: '241',
      g: '112',
      b: '19',
      a: '1',
		},
		backgroundColor: "",
		fileNames: [],
		isBrandLogo: true,
  };

	handleChange = (name, e) => {
		this.setState({ [name]: e.target.value });
	}
	
	addRow = () => {
    this.setState(previousState => {
      return {
        rows: [...previousState.rows, { name : "", image : ""}]
      };
    });
  };

  removeRow = index => {
    this.setState(previousState => {
      const rows = [...previousState.rows];
      rows.splice(index, 1);
      return { rows };
    });
  };

  onChange = (event, index) => {
    const { name, value } = event.target;

    this.setState(previousState => {
      const rows = [...previousState.rows];
      rows[index] = { ...rows[index], [name]: value };
      return { rows };
    });
	};
	
	handleClickColor = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };

  handleCloseColor = () => {
    this.setState({ displayColorPicker: false })
  };

  handleChangeColor = (color) => {
		var newColor = this.rgbToHex(color.rgb.r, color.rgb.g, color.rgb.b);
    this.setState({ color : color.rgb, backgroundColor : newColor })
	};
	
	hexToRgb = (hex) => {
		hex = '0x' + hex
		return {
			r: (hex >> 16) & 0xFF,
			g: (hex >> 8) & 0xFF,
			b: hex & 0xFF,
			a: 1
		};
	};
	
	componentToHex = (c) => {
		var hex = c.toString(16);
		return hex.length === 1 ? "0" + hex : hex;
	};

	rgbToHex = (r, g, b) => {
    return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
	};
	
	handleChangeColorInput = (event) => {
		var newColor = event.target.value.replace(/^#/, "");
		newColor = this.hexToRgb(newColor);
		this.setState({ color: newColor, backgroundColor : event.target.value });
	};

	onFileChange = (event) => {
		var fileNames = [];
		for(var i = 0; i < event.target.files.length; i++){
			fileNames.push(event.target.files[i].name)
		}
		this.setState({ fileNames })
	}

	handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

	render() {
		const styles = reactCSS({
      'default': {
        color: {
          width: '36px',
					height: '30px',
          background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
        },
        swatch: {
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
					position: 'absolute',
          zIndex: '2',
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });

		return (
			<div>
        <Sidebar active="create-game">
        </Sidebar>
        
        <Topbar>
        </Topbar>

        <div className="content">
          <div className="smallHeader">Game</div>
          <div className="bigHeader">Customisation</div>
          <Row className="mt-5">
            <Col>
              <div className="input-label text-gray">NAME</div>
              <input className="cc-input" type="text"/>

              <div className="input-label text-gray">GAME TYPE</div>
              <select className="cc-input" value={this.state.gameType} onChange={ (e) => this.handleChange("gameType", e)}>
								<option value=""></option>
								<option value="survey">Gamify Survey</option>
                <option value="claw">Claw Machine</option>
              </select>

							<div className="input-label text-gray mt-4">
								<span className="mr-3">
									UPLOAD IMAGE
								</span>

								<input type="file" id="file" name="file" className="inputfile" onChange={this.onFileChange} multiple/>
								<label htmlFor="file">Choose a file</label>
							</div>
							<div className="text-gray text-14">(Supported image type: .png/.jpg)</div>
							<div className="text-gray text-14 mb-4">(Recommended resolution: 400x400px)</div>

							<div className="mb-3 text-blue text-16">
								{
									this.state.fileNames.map(name => (
										<div>{name}</div>
									))
								}
							</div>

							<div className="input-label text-gray">UPLOAD BACKGROUND</div>
							<div className="d-flex">
								<input className="cc-input" type="text" value={this.state.backgroundColor} onChange={this.handleChangeColorInput}/>
								<div className="colorpicker">
									<div className="divColorPicker" onClick={ this.handleClickColor }>
										<div className="divColorPicked" />
									</div>
									<div style={ styles.swatch } onClick={ this.handleClickColor }>
										<div style={ styles.color } />
										{ this.state.displayColorPicker ? <div style={ styles.popover }>
											<div style={ styles.cover } onClick={ this.handleCloseColor }/>
											<SketchPicker color={ this.state.color } onChange={ this.handleChangeColor } />
										</div> : null }
									</div>
								</div>
							</div>

							{this.state.gameType === "survey" ? 
								<div>
									<div className="input-label text-gray">QUESTION</div>
									<input className="cc-input" type="text"/>

									{this.state.rows.map((row, index) => (
										<div key={index}>
											<div className="input-label text-gray">ANSWER #{index+1}</div>
											<div className="d-flex"> 
												<input className="cc-input pr-40 mr-1" type="text" onChange={e => this.onChange(e, index)} value={row.name}/>
												<select className="cc-input" value={this.state.gameType} onChange={ (e) => this.handleChange("field1", e)}>
													
													{
														this.state.fileNames.map((name, index) => (
															<option value={index}>{name}</option>
														))
													}
												
												</select>
											</div>
										</div>
									))}

									<div className="input-label text-gray c-pointer" onClick={this.addRow} >Add Reward</div>

									<div className="input-label text-gray"> DATABASE NAME</div>
									<input className="cc-input" type="text"/>

									<Row className="align-items-center">
										<Col className="text-gray text-14">
											BRAND LOGO TOGGLE
										</Col>
										<Col className="mw-fit">
											<label className="switch">
												<input type="checkbox" checked/>
												<span className="slider round"></span>
											</label>
										</Col>
									</Row>
								</div> 
							: (this.state.gameType === "claw" ? 
								<div>
									<div className="input-label text-gray">CAMPAIGN</div>
									<input className="cc-input" type="text"/>

									{this.state.rows.map((row, index) => (
										<div key={index}>
											<div className="input-label text-gray">Reward Name #{index+1}</div>
											<div className="d-flex"> 
												<input className="cc-input pr-40 mr-1" type="text" onChange={e => this.onChange(e, index)} value={row.name}/>
												<select className="cc-input" value={this.state.gameType} onChange={ (e) => this.handleChange("field1", e)}>
													{
														this.state.fileNames.map((name, index) => (
															<option value={index}>{name}</option>
														))
													}
												</select>
											</div>
										</div>
									))}

									<div className="input-label text-gray c-pointer" onClick={this.addRow} >Add Reward</div>

									<div className="input-label text-gray mt-3"> DATABASE NAME</div>
									<input className="cc-input" type="text"/>

									<Row className="align-items-center">
										<Col className="text-gray text-14">
											BRAND LOGO TOGGLE
										</Col>
										<Col className="mw-fit">
											<label className="switch">
												<input type="checkbox" name="isBrandLogo" onClick={this.handleInputChange} checked={this.state.isBrandLogo}/>
												<span className="slider round"></span>
											</label>
										</Col>
									</Row>
								</div>
							: "")}
            </Col>
            <Col>
							{this.state.gameType === "survey" ? 
								<Image className="game-images" src={surveyQuizImage}/>
								: this.state.gameType === "claw" ? 
									<Image className="game-images" src={clawMachineImage}/>
									: ""
							}
            </Col>
          </Row>
					<div className="buttons-div float-right">
						<Button className="bg-light-blue" variant="primary">PREVIEW</Button>
						<Button className="bg-green" variant="primary">Done</Button>
					</div>
        </div>
      </div>
		)
	}
}

export default CreateGame;