import React, { Component } from 'react';
import { Row, Col, Table} from 'react-bootstrap';
import Sidebar from './Sidebar';
import Topbar from './Topbar';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

export class ManageRewards extends Component {
	state = {
		rewards: [
			{
				id : 0,
				rewardName : "Winning Prize #1",
				code : "15%OFF",
				used : "4/100",
				status : "Active",
				startDate :  "15/03/2019",
				endDate : "15/04/2020",
				coupons :[
					{
						id : 0,
						uniqueCode : "15%OFFWKJ",
						time : "20:31",
						usedDate : "18/03/2020"
					},
					{
						id : 1,
						uniqueCode : "15%OFFWKJ",
						time : "20:31",
						usedDate : "18/03/2020"
					},
					{
						id : 2,
						uniqueCode : "15%OFFWKJ",
						time : "20:31",
						usedDate : "18/03/2020"
					},
				]
			},
			{
				id : 1,
				rewardName : "Winning Prize #2",
				code : "15%OFF",
				used : "4/100",
				status : "Active",
				startDate :  "15/03/2019",
				endDate : "15/04/2020",
				coupons :[
					{
						id : 0,
						uniqueCode : "15%OFFWKJ",
						time : "20:31",
						usedDate : "18/03/2020",
					},
					{
						id : 1,
						uniqueCode : "15%OFFWKJ",
						time : "20:31",
						usedDate : "18/03/2020"
					},
					{
						id : 2,
						uniqueCode : "15%OFFWKJ",
						time : "20:31",
						usedDate : "18/03/2020"
					},
				]
			},
		],
		exportData: [
		]
	};

	componentDidMount() {
		const rewards = this.state.rewards;
		for(var j = 0; j<rewards.length; j++){
			rewards[j].isAddedAll = false;
			for(var k = 0; k<rewards[j].coupons.length; k++){
				rewards[j].coupons[k].isAdded = false;
			}
		}
		this.setState({
      rewards
		});
  }
	
	handleChecklistChange = (event, indexReward) => {
		const target = event.target;
		const rewards = this.state.rewards;
		const exportData = this.state.exportData;
		var isAllTrue = true;
		var isAnyTrue = false;

		rewards[indexReward].coupons[target.value].isAdded = !rewards[indexReward].coupons[target.value].isAdded;
		const index = exportData.findIndex(element => element.id === rewards[indexReward].id);

		if( index >= 0){
			const indexRowExport = exportData[index].coupons.findIndex(element => element.id === rewards[indexReward].coupons[target.value].id);
			if(indexRowExport < 0){
				exportData[index].coupons.push(rewards[indexReward].coupons[target.value]);
			}
		} else {
			exportData.push(rewards[indexReward]);
		}

		for(var l = 0; l<rewards[indexReward].coupons.length; l++){
			isAllTrue = (isAllTrue && rewards[indexReward].coupons[l].isAdded);

			if(rewards[indexReward].coupons[l].isAdded){
				isAnyTrue = true;
			}
		}

		if(isAllTrue){
			rewards[indexReward].isAddedAll = true;
		} else {
			rewards[indexReward].isAddedAll = false;
		}

		if(!isAnyTrue){
			exportData.splice(index, 1);
		}

		this.setState(
			{
				rewards,
				exportData
			}
		);
	};

	handleChecklistChangeAll = (event) => {
		const target = event.target;
		const rewards = this.state.rewards;
		const exportData = this.state.exportData;

		rewards[target.value].isAddedAll = !rewards[target.value].isAddedAll; 

		const index = exportData.findIndex(element => element.id === rewards[target.value].id);
		if(rewards[target.value].isAddedAll){
			rewards[target.value].coupons.map((coupon,i) =>(
				coupon.isAdded = true
			));

			if( index >= 0){
				exportData[index].coupons = rewards[target.value].coupons;
			} else {
				exportData.push(rewards[target.value]);
			}

		} else {
			exportData.splice(index,1);

			rewards[target.value].coupons.map((coupon,i) =>(
				coupon.isAdded = false
			));
		}

		
		this.setState(
			{
				rewards,
				exportData
			}
		);
	}
	
	render() {
		return (
			<div>
        <Sidebar active="manage-campaign">
        </Sidebar>
        
        <Topbar>
        </Topbar>

        <div className="content">
					<Row>
            <Col lg={6}>
							<div className="smallHeader">Rewards</div>
							<div className="bigHeader">Manage Rewards</div>
							<div className="campaign-name mt-2">Spring Sale</div>
						</Col>
						<Col lg={6}>
							<ReactHTMLTableToExcel
								id="test-table-xls-button"
								className="download-table-xls-button"
								table="table-to-xls"
								filename="rewardCoupons"
								sheet="tablexls"
								buttonText="Export Data"/>
						</Col>
					</Row>
					<Table id="table-to-xls" className="d-none">
						<thead>
							<tr>
								<th>
									REWARDS NAME
								</th>
								<th>
									CODE
								</th>
								<th>
									USED
								</th>
								<th>
									STATUS
								</th>
								<th>
									START DATE
								</th>
								<th>
									END DATE
								</th>
							</tr>
						</thead>

						{
							this.state.exportData.map((reward,i) =>
								<tbody key={i}>
									<tr>
										<td>{reward.rewardName}</td>
										<td>{reward.code}</td>
										<td>{reward.used}</td>
										<td>{reward.status}</td>
										<td>{reward.startDate}</td>
										<td>{reward.endDate}</td>
									</tr>
									
									<tr>
										<th colSpan="4">UNIQUE CODE</th>
										<th>TIME</th>
										<th>USED DATE</th>
									</tr>

									{
										reward.coupons.map((coupon, j)=>
											coupon.isAdded &&
												<tr key={j}>
													<td colSpan="4">{coupon.uniqueCode}</td>
													<td>{coupon.time}</td>
													<td>{coupon.usedDate}</td>
												</tr>
										)
									}
								</tbody>
							)
						}
					</Table>

          <Row className="mt-3">
            <Col>
              <div className="input-label mb-0 text-gray text-center">REWARDS NAME</div>
            </Col>
            <Col>
							<div className="input-label mb-0 text-gray text-center">CODE</div>
            </Col>
						<Col>
							<div className="input-label mb-0 text-gray text-center">USED</div>
            </Col>
						<Col>
							<div className="input-label mb-0 text-gray text-center">STATUS</div>
            </Col>
						<Col>
							<div className="input-label mb-0 text-gray text-center">START DATE</div>
            </Col>
						<Col>
							<div className="input-label mb-0 text-gray text-center">END DATE</div>
            </Col>
          </Row>
					{
						this.state.rewards.map((reward,i) =>(
							<div key={i}>
								<Row className={i>0?"mt-4 bg-white border-gray align-items-center" : "mt-1 bg-white border-gray align-items-center"}>
									<input className="ml-1" type="checkbox" name={"all-"+i} value={i} onChange={this.handleChecklistChangeAll} checked={reward.isAddedAll === null ? false : reward.isAddedAll}/>
									<Col className="first-col-negative-margin-reward px-0">
										<div className="input-label mb-0 text-center">{reward.rewardName}</div>
									</Col>
									<Col>
										<div className="input-label mb-0 text-center">{reward.code}</div>
									</Col>
									<Col>
										<div className="input-label mb-0 text-center">{reward.used}</div>
									</Col>
									<Col>
										<div className={reward.status==="Active"?"input-label mb-0 text-center active-tag":reward.status==="Inactive"?"input-label mb-0 text-center inactive-tag":"input-label mb-0 text-center"}>{reward.status}</div>
									</Col>
									<Col>
										<div className="input-label mb-0 text-center">{reward.startDate}</div>
									</Col>
									<Col>
										<div className="input-label mb-0 text-center">{reward.endDate}</div>
									</Col>
								</Row>
								{
									<div className="ml-2 mr-2">
										<Row className="bg-white border-gray align-items-center py-1">
											<Col className="pl-4" lg="6">
												<div className="input-label mb-0 text-gray">UNIQUE CODE</div>
											</Col>
											<Col lg="3">
												<div className="input-label mb-0 text-center text-gray">TIME</div>
											</Col>
											<Col lg="3">
												<div className="input-label mb-0 text-center text-gray">USED DATE</div>
											</Col>
										</Row>

										{
											reward.coupons.map((coupons,l)=>
												<Row className="bg-white border-gray align-items-center" key={l}>
													<input className="ml-1 always-front" type="checkbox" name={i+"-"+l} value={l} onChange={(e) => this.handleChecklistChange(e, i)} checked={coupons.isAdded}/>
													<Col className="first-col-negative-margin" lg="6">
														<div className="input-label mb-0 text-blue pl-4">{coupons.uniqueCode}</div>
													</Col>
													<Col lg="3">
														<div className="input-label mb-0 text-center">{coupons.time}</div>
													</Col>
													<Col lg="3">
														<div className="input-label mb-0 text-center">{coupons.usedDate}</div>
													</Col>
												</Row>
											)
										}
									</div>
								}
						</div>
					 ))
					}
        </div>
      </div>
		)
	}
}

export default ManageRewards;