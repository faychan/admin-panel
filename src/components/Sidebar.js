import React, { Component } from 'react';
import logoBuyble from '../img/Logo_W.png';
import profilePicture from '../img/profile-picture.jpg';

export class Sidebar extends Component {

	render() {
		return (
			<div className="sidebar">
				<div className="logo-container">
					<img alt="Buyble Logo" className="pl-30 logo-buyble" src={logoBuyble} />
				</div>
				<div className="profile">
					<img alt="Profile" className="profile-picture" src={profilePicture} />
					<div className="profile-info">
						<div className="profile-name">Brian Anderson</div>
						<div className="profile-position">Admin</div>
					</div>
				</div>
				<div className="sidebar-category">General</div>
				<a className={this.props.active==="dashboard"?"active pl-30" : "pl-30"} href="/">
					Dashboard
				</a>
				<a className="pl-30 pt-0" href="#">
					Stores
				</a>
				<div className="sidebar-category">Games</div>
				<a className={this.props.active==="create-game"?"active pl-30" : "pl-30"} href="/create-game">
					Create Game
				</a>
				<a className={this.props.active==="manage-game"?"active pl-30" : "pl-30"} href="/manage-game">
					Manage Game
				</a>
				<div className="sidebar-category">Rewards</div>
				<a className={this.props.active==="create-campaign"?"active pl-30" : "pl-30"} href="/create-campaign">
					Create Rewards
				</a>
				<a className={this.props.active==="manage-campaign"?"active pl-30" : "pl-30"} href="/manage-campaign">
					Manage Rewards
				</a>
			</div>
		)
	}
}

export default Sidebar;