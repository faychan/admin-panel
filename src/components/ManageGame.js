import React, { Component } from 'react';
import { Row, Col, Button, Image} from 'react-bootstrap';
import Sidebar from './Sidebar';
import Topbar from './Topbar';
import fortuneWheelImage from '../img/fortune-wheel.png';
import craneMachineImage from '../img/crane-machine.png';
import reviewQuiz from '../img/review-us.png';


export class ManageGame extends Component {

	render() {
		return (
			<div>
        <Sidebar active="manage-game">
        </Sidebar>
        
        <Topbar>
        </Topbar>

        <div className="content">
          <div className="smallHeader">Game</div>
          <div className="bigHeader">Manage</div>
          <Row className="mt-5">
            <Col>
							<Image className="game-images" src={craneMachineImage} thumbnail/>
							<Button className="game-button" variant="outline-primary" size="lg" block>
								Claw Machine
							</Button>
            </Col>
            <Col>
							<Image className="game-images" src={fortuneWheelImage} thumbnail/>
							<Button className="game-button" variant="outline-primary" size="lg" block>
								Wheel of Chance
							</Button>
            </Col>
          </Row>
          <Row className="mt-5">
            <Col md={6}>
							<Image className="game-images" src={reviewQuiz} thumbnail/>
							<Button className="game-button" variant="outline-primary" size="lg" block>
								Gamify Survey
							</Button>
            </Col>
          </Row>
        </div>
      </div>
		)
	}
}

export default ManageGame;