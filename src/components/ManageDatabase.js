import React, { Component } from 'react';
import { Row, Col, Table} from 'react-bootstrap';
import Sidebar from './Sidebar';
import Topbar from './Topbar';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

export class ManageDatabase extends Component {
	state = {
		database: [
			{
				id : 0,
				answerName : "Answer 1",
				noAnswer : "231",
				percentage : "36.1",
			},
			{
				id : 1,
				answerName : "Answer 2",
				noAnswer : "132",
				percentage : "20.6",
			},
			{
				id : 2,
				answerName : "Answer 3",
				noAnswer : "277",
				percentage : "43.3",
			}
		],
		isAddedAll: false,
		exportData: [
		]
	};

	componentDidMount() {
		const database = this.state.database;
		for(var j = 0; j<database.length; j++){
			database[j].isAdded = false;
		}
		this.setState({
      database
		});
  }
	
	handleChecklistChange = (event) => {
		const target = event.target;
		const database = this.state.database;
		const exportData = this.state.exportData;
		var isAddedAll = this.state.isAddedAll;
		var isAllTrue = true;
		var isAnyTrue = false;

		console.log(database, target.value)

		database[target.value].isAdded = !database[target.value].isAdded;
		const index = exportData.findIndex(element => element.id === database[target.value].id);

		if( index < 0){
			exportData.push(database[target.value]);
		}

		for(var l = 0; l<database.length; l++){
			isAllTrue = (isAllTrue && database[l].isAdded);

			if(database[target.value].isAdded){
				isAnyTrue = true;
			}
		}

		isAllTrue ? isAddedAll = true :  isAddedAll = false;

		if(!isAnyTrue){
			exportData.splice(index, 1);
		}

		this.setState(
			{
				database,
				exportData,
				isAddedAll,
			}
		);
	};

	handleChecklistChangeAll = (event) => {
		const database = this.state.database;
		var exportData = this.state.exportData;
		var isAddedAll = this.state.isAddedAll;

		isAddedAll = !isAddedAll; 

		database.map(data =>(
			data.isAdded = isAddedAll
		));

		exportData = database;
		
		this.setState(
			{
				database,
				exportData,
				isAddedAll,
			}
		);
	}
	
	render() {
		return (
			<div>
        <Sidebar active="manage-campaign">
        </Sidebar>
        
        <Topbar>
        </Topbar>

				<div className="content">
					<Row>
						<Col lg={6}>
							<div className="smallHeader">Game</div>
							<div className="bigHeader">Manage Database</div>
							<div className="campaign-name mt-2">Survey_data</div>
						</Col>
						<Col lg={6}>
							<ReactHTMLTableToExcel
							id="test-table-xls-button"
							className="download-table-xls-button"
							table="table-to-xls"
							filename="database"
							sheet="tablexls"
							buttonText="Export data"/>
						</Col>
					</Row>

					<Table id="table-to-xls" className="d-none">
						<thead>
							<tr>
								<th>
									ANSWER NAME
								</th>
								<th>
									NO. ANSWER
								</th>
								<th>
									PERCENTAGE
								</th>
							</tr>
						</thead>

						{
							this.state.exportData.map((data,i) =>
								data.isAdded &&
									<tbody key={i}>
										<tr>
											<td>{data.answerName}</td>
											<td>{data.noAnswer}</td>
											<td>{data.percentage}</td>
										</tr>
									</tbody>
							)
						}
					</Table>

          <Row className="mt-3 align-items-center">
						<input className="ml-1" type="checkbox" name="all" value="all" onChange={this.handleChecklistChangeAll} checked={this.state.isAddedAll}/>
            <Col>
              <div className="input-label mb-0 text-gray text-center">ANSWER NAME</div>
            </Col>
            <Col>
							<div className="input-label mb-0 text-gray text-center">NO. ANSWER</div>
            </Col>
						<Col>
							<div className="input-label mb-0 text-gray text-center">PERCENTAGE (%)</div>
            </Col>
          </Row>
					{
						this.state.database.map((data,i) =>(
							<div key={i}>
								<Row className={i>0?"mt-4 bg-white border-gray align-items-center py-1" : "mt-1 bg-white border-gray align-items-center py-1"}>
									<input className="ml-1 always-front" type="checkbox" value={i} onChange={this.handleChecklistChange} checked={data.isAdded}/>
									<Col className="first-col-negative-margin">
										<div className="input-label mb-0 text-center pl-4">{data.answerName}</div>
									</Col>
									<Col>
										<div className="input-label mb-0 text-center pl-4">{data.noAnswer}</div>
									</Col>
									<Col>
										<div className="input-label mb-0 text-center pl-4">{data.percentage}</div>
									</Col>
								</Row>
							</div>
						))
					}
        </div>
      </div>
		)
	}
}

export default ManageDatabase;