import React, { Component } from 'react';
import { Row, Col} from 'react-bootstrap';
import Sidebar from './Sidebar';
import Topbar from './Topbar';

export class ManageCampaign extends Component {
	
	render() {
		return (
			<div>
        <Sidebar active="manage-campaign">
        </Sidebar>
        
        <Topbar>
        </Topbar>

        <div className="content">
          <div className="smallHeader">Rewards</div>
          <div className="bigHeader">Manage Campaign</div>
          <Row className="mt-5">
            <Col>
              <div className="input-label text-gray text-center">CAMPAIGN NAME</div>
            </Col>
            <Col>
							<div className="input-label text-gray text-center">NUMBER OF REWARDS</div>
            </Col>
						<Col>
							<div className="input-label text-gray text-center">START DATE</div>
            </Col>
						<Col>
							<div className="input-label text-gray text-center">END DATE</div>
            </Col>
          </Row>
					<Row className="mt-1 bg-white border-gray align-items-center">
            <Col>
							<a className="" href="/manage-rewards">	
								<div className="input-label text-center">
										Spring Sale
								</div>
							</a>
            </Col>
            <Col>
							<div className="input-label text-center">1</div>
            </Col>
						<Col>
							<div className="input-label text-center">15/03/2019</div>
            </Col>
						<Col>
							<div className="input-label text-center">15/04/2020</div>
            </Col>
          </Row>
					<Row className="mt-5 bg-white border-gray align-items-center">
            <Col>
              <div className="input-label text-center">Summer Sale</div>
            </Col>
            <Col>
							<div className="input-label text-center">4</div>
            </Col>
						<Col>
							<div className="input-label text-center">09/06/2020</div>
            </Col>
						<Col>
							<div className="input-label text-center">09/15/2020</div>
            </Col>
          </Row>
					<Row className="mt-5 bg-white border-gray align-items-center">
            <Col>
              <div className="input-label text-center">Back to School Sale</div>
            </Col>
            <Col>
							<div className="input-label text-center">5</div>
            </Col>
						<Col>
							<div className="input-label text-center">04/09/2020</div>
            </Col>
						<Col>
							<div className="input-label text-center">20/09/2020</div>
            </Col>
          </Row>
        </div>
      </div>
		)
	}
}

export default ManageCampaign;