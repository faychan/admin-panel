import React, { Component } from 'react';

export class Topbar extends Component {

	render() {
		return (
			<div className="topbar">
				<div className="searchbar-container">
					<i className="fas fa-search" />
					<input type="text" placeholder="Search.." />
					<select>
						<option value="thisPage">This page</option>
					</select>
				</div>

				<div className="logout-button">
					Logout
				</div>
			</div>
		)
	}
}

export default Topbar;