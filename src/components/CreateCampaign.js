import React, { Component } from 'react';
import { Row, Col, Button} from 'react-bootstrap';
import reactCSS from 'reactcss';
import { SketchPicker } from 'react-color';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import Sidebar from './Sidebar';
import Topbar from './Topbar';

export class ManageGame extends Component {
  state = {
    campaignName: "",
    font: "",
    celebrationText: "",
    inputColorCelebration: "",
    inputColorBackground: "",
    displayColorPickerCelebration: false,
    displayColorPickerBackground: false,
    rows: [{
      name : "",
      coupon : "",
      details : "",
      start : "",
      end : "",
      number : "",
      colorName: {
        r: '0',
        g: '0',
        b: '0',
        a: '1',
      },
      colorNameHex: "",
      colorDetail: {
        r: '0',
        g: '0',
        b: '0',
        a: '1',
      },
      colorDetailHex: "",
      displayColorPickerName: false,
      displayColorPickerDetail: false,
    }],
    colorCelebration: {
      r: '0',
      g: '0',
      b: '0',
      a: '1',
    },
    colorBackground: {
      r: '231',
      g: '231',
      b: '255',
      a: '1',
    },
  };

  addRow = () => {
    this.setState(previousState => {
      return {
        rows: [...previousState.rows, { 
          name : "", 
          coupon : "", 
          details : "", 
          start : "", 
          end : "", 
          number : "", 
          colorName: {
            r: '241',
            g: '112',
            b: '19',
            a: '1',
          },
          colorNameHex: "",
          colorDetail: {
            r: '241',
            g: '112',
            b: '19',
            a: '1',
          },
          colorDetailHex: "",
          displayColorPickerName: false,
          displayColorPickerDetail: false,
        }]
      };
    });
  };

  removeRow = index => {
    this.setState(previousState => {
      const rows = [...previousState.rows];
      rows.splice(index, 1);
      return { rows };
    });
  };

  onChangeRow = (event, index) => {
    const { name, value } = event.target;
    this.setState(previousState => {
      const rows = [...previousState.rows];
      rows[index] = { ...rows[index], [name]: value };
      return { rows };
    });
  };

  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  hexToRgb = (hex) => {
		hex = '0x' + hex
		return {
			r: (hex >> 16) & 0xFF,
			g: (hex >> 8) & 0xFF,
			b: hex & 0xFF,
			a: 1
		};
	};
	
	componentToHex = (c) => {
		var hex = c.toString(16);
		return hex.length === 1 ? "0" + hex : hex;
	};

	rgbToHex = (r, g, b) => {
    return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
	};

  onInputColorChange = (event, colorPreview, colorInput) => {
    var newColor = event.target.value.replace(/^#/, "");
		newColor = this.hexToRgb(newColor);
    this.setState({ [colorPreview]: newColor, [colorInput] : event.target.value });
  };

  handleClickColor = (index, location, name) => {
    if(location === "inside"){
      this.setState(previousState => {
        const rows = [...previousState.rows];
        rows[index] = { ...rows[index], [name]: !rows[index][name] };
        return { rows };
      });
    } else {
      this.setState({ [name]: !this.state[name] })
    }
  };

  handleCloseColor = (index, location, name) => {
    if(location === "inside"){
      this.setState(previousState => {
        const rows = [...previousState.rows];
        rows[index] = { ...rows[index], [name]: false };
        return { rows };
      });
    } else {
      this.setState({ [name]: false })
    }
  };

  handleChangeColor = (color, index, location , rgbColor, hexColor) => {
    var newColor = this.rgbToHex(color.rgb.r, color.rgb.g, color.rgb.b);
    if(location === "inside"){
      this.setState(previousState => {
        const rows = [...previousState.rows];
        rows[index] = { ...rows[index], [rgbColor] : color.rgb,  [hexColor] : newColor};
        return { rows };
      });
    } else {
      this.setState({ [rgbColor] : color.rgb,  [hexColor] : newColor})
    }
  };
  
	render() {
    const styles = reactCSS({
      'default': {
        swatch: {
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
					position: 'absolute',
          zIndex: '2',
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });

		return (
			<div>
        <Sidebar active="create-campaign">
        </Sidebar>
        
        <Topbar>
        </Topbar>

        <div className="content">
          <div className="smallHeader">Create</div>
          <div className="bigHeader">Campaign</div>
          <Row className="mt-5">
            <Col>
              <div className="input-label text-gray">Campaign Name</div>
              <input name="campaignName" className="cc-input" type="text" onChange={this.onChange} value={this.state.campaignName}/>

              <div className="input-label text-gray">Font</div>
              <select name="font" className="cc-input" onChange={this.onChange} value={this.state.font}>
                <option value="arial">Arial</option>
                <option value="papyrus">Papyrus</option>
              </select>

              {this.state.rows.map((row, index) => (
                <div key={row}>
                  <div className="input-label text-gray">Reward Name #{index+1}</div>
                  <div className="d-flex">
                    <input className="cc-input pr-40" name="name" type="text" onChange={e => this.onChangeRow(e, index)} value={row.name}/>
                    <div className="colorpicker">
                      <div className="divColorPicker" onClick={ () => this.handleClickColor(index, 'inside', 'displayColorPickerName') }>
                        <div className="divColorPicked" />
                      </div>
                      <div style={ styles.swatch } onClick={ () => this.handleClickColor(index, 'inside', 'displayColorPickerName')}>
                        <div style={{width: '36px', height: '30px',background: `rgba(${ this.state.rows[index].colorName.r }, ${ this.state.rows[index].colorName.g }, ${ this.state.rows[index].colorName.b }, ${ this.state.rows[index].colorName.a })`}} />
                        { row.displayColorPickerName ? <div style={ styles.popover }>
                          <div style={ styles.cover } onClick={ () => this.handleCloseColor(index, 'inside', 'displayColorPickerName')}/>
                          <SketchPicker color={ row.colorName } onChange={ e => this.handleChangeColor(e, index , 'inside', 'colorName', 'colorNameHex')} />
                        </div> : null }
                      </div>
                    </div>
                  </div>
          
                  <div className="input-label text-gray">Coupon</div>
                  <input name="coupon" className="cc-input" type="text" onChange={e => this.onChangeRow(e, index)} value={row.coupon}/>
          
                  <div className="input-label text-gray">Reward Details</div>
                  <div className="d-flex">
                    <textarea name="details" className="cc-input pr-40" onChange={e => this.onChangeRow(e, index)} value={row.details}> </textarea>
                    <div className="colorpicker">
                      <div className="divColorPicker" onClick={ () => this.handleClickColor(index, 'inside', 'displayColorPickerDetail') }>
                        <div className="divColorPicked" />
                      </div>
                      <div style={ styles.swatch } onClick={ () => this.handleClickColor(index, 'inside', 'displayColorPickerDetail')}>
                        <div style={{width: '36px', height: '30px',background: `rgba(${ this.state.rows[index].colorDetail.r }, ${ this.state.rows[index].colorDetail.g }, ${ this.state.rows[index].colorDetail.b }, ${ this.state.rows[index].colorDetail.a })`}} />
                        { row.displayColorPickerDetail ? <div style={ styles.popover }>
                          <div style={ styles.cover } onClick={ () => this.handleCloseColor(index, 'inside', 'displayColorPickerDetail')}/>
                          <SketchPicker color={ row.colorDetail } onChange={ e => this.handleChangeColor(e, index , 'inside', 'colorDetail', 'colorDetailHex')} />
                        </div> : null }
                      </div>
                    </div>
                  </div>

                  <Row>
                    <Col>
                      <div className="input-label text-gray">Start Date</div>
                      <input name="start" className="cc-input" type="date" onChange={e => this.onChangeRow(e, index)} value={row.start}/>
                    </Col>
                    <Col>
                      <div className="input-label text-gray">End Date</div>
                      <input name="end" className="cc-input" type="date" onChange={e => this.onChangeRow(e, index)} value={row.end}/>
                    </Col>
                  </Row>
          
                  <div className="input-label text-gray">Number of Coupon</div>
                  <input name="number" className="cc-input" type="number" onChange={e => this.onChangeRow(e, index)} value={row.number}/>
                </div>
              ))}

              <div className="input-label text-gray c-pointer" onClick={this.addRow} >Add Reward</div>

							<div className="input-label mt-5 text-gray">Celebration Text</div>
              <div className="d-flex">
                <div className="w-100">
                  <input name="celebrationText" className="cc-input pr-40" type="text" onChange={this.onChange} value={this.state.celebrationText} />
                  <input name="inputColorCelebration" className="cc-input celebrationColorInput" type="text" onChange={(e) => this.onInputColorChange(e, "colorCelebration", "inputColorCelebration")} value={this.state.inputColorCelebration}/>
                </div>
                <div className="colorpicker">
                  <div className="divColorPicker" onClick={ () => this.handleClickColor(0, "outside", 'displayColorPickerCelebration') }>
                    <div className="divColorPicked" />
                  </div>
                  <div style={ styles.swatch } onClick={ () => this.handleClickColor(0, "outside", 'displayColorPickerCelebration')}>
                    <div style={{width: '36px', height: '30px',background: `rgba(${ this.state.colorCelebration.r }, ${ this.state.colorCelebration.g }, ${ this.state.colorCelebration.b }, ${ this.state.colorCelebration.a })`}} />
                    { this.state.displayColorPickerCelebration ? <div style={ styles.popover }>
                      <div style={ styles.cover } onClick={ () => this.handleCloseColor(0, "outside", 'displayColorPickerCelebration')}/>
                      <SketchPicker color={ this.state.colorCelebration } onChange={ e => this.handleChangeColor(e, 0, "outside", 'colorCelebration', "inputColorCelebration")} />
                    </div> : null }
                  </div>
                </div>
              </div>

              <div className="input-label text-gray">Background Color</div>
              <div className="d-flex">
                <input name="inputColorBackground" className="cc-input pr-40" type="text" onChange={(e) => this.onInputColorChange(e, "colorBackground", "inputColorBackground")} value={this.state.inputColorBackground}/>
                <div className="colorpicker">
                  <div className="divColorPicker" onClick={ () => this.handleClickColor(0, 'outside', 'displayColorPickerBackground') }>
                    <div className="divColorPicked" />
                  </div>
                  <div style={ styles.swatch } onClick={ () => this.handleClickColor(0, 'outside', 'displayColorPickerBackground')}>
                    <div style={{width: '36px', height: '30px',background: `rgba(${ this.state.colorBackground.r }, ${ this.state.colorBackground.g }, ${ this.state.colorBackground.b }, ${ this.state.colorBackground.a })`}} />
                    { this.state.displayColorPickerBackground ? <div style={ styles.popover }>
                      <div style={ styles.cover } onClick={ () => this.handleCloseColor(0, 'outside', 'displayColorPickerBackground')}/>
                      <SketchPicker color={ this.state.colorBackground } onChange={ e => this.handleChangeColor(e, 0, "outside", 'colorBackground', "inputColorBackground")} />
                    </div> : null }
                  </div>
                </div>
              </div>
              
              <Row className="align-items-center">
                <Col className="text-gray text-14">
                  BRAND LOGO TOGGLE
                </Col>
                <Col className="mw-fit">
                  <label className="switch">
                    <input type="checkbox" checked/>
                    <span className="slider round"></span>
                  </label>
                </Col>
              </Row>
              <Row className="align-items-center">
                <Col className="text-gray text-14">
                  UNIQUE COUPON GENERATION
                </Col>
                <Col className="mw-fit">
                  <label className="switch">
                    <input type="checkbox" checked/>
                    <span className="slider round"></span>
                  </label>
                </Col>
              </Row>
            </Col>
            <Col>
              <div style={{backgroundColor:this.state.inputColorBackground}}  className="couponContainer">

                <div style={{color:this.state.inputColorCelebration}} className="couponText mb-2">
                  {
                    this.state.celebrationText === "" ? "CELEBRATION TEXT" : this.state.celebrationText
                  }
                </div>

                {
                  this.state.rows.map((value,index)=> (
                    <div key={index} className={index>0?"mt-3":""}>
                      <div style={{color:value.colorNameHex}} className="couponName mb-2">{value.name===""? "REWARD NAME" : value.name}</div>
                      <div style={{color:value.colorDetailHex}} className="couponDetails mb-2">{value.details===""? "REWARD DETAILS" : value.details}</div>
                      <div>
                        <input type="text" className="couponInput text-center mr-2" value={value.coupon}></input>
                        {
                          value.coupon !== "" ? 
                            <CopyToClipboard text={value.coupon} onCopy={() => alert("Copied!")}>
                              <span>
                                <i className="fas fa-copy c-pointer"></i>
                              </span>
                            </CopyToClipboard>
                            : ""
                        }
                      </div>
                    </div>
                  ))
                }
              </div>
            </Col>
          </Row>
          <div className="buttons-div float-right">
						<Button className="mr-0 bg-light-blue" variant="primary">PREVIEW</Button>
					</div>
        </div>
      </div>
		)
	}
}

export default ManageGame;