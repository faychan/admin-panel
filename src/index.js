import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import App from './App'
import ManageGame from './components/ManageGame';
import CreateGame from './components/CreateGame';
import CreateCampaign from './components/CreateCampaign';
import ManageCampaign from './components/ManageCampaign';
import ManageRewards from './components/ManageRewards';
import ManageDatabase from './components/ManageDatabase';

const routing = (
  <Router>
    <div>
      <Route exact path="/" component={App} />
      <Route path="/manage-game" component={ManageGame} />
      <Route path="/create-game" component={CreateGame} />
      <Route path="/create-campaign" component={CreateCampaign} />
      <Route path="/manage-campaign" component={ManageCampaign} />
      <Route path="/manage-rewards" component={ManageRewards} />
    </div>
  </Router>
)
ReactDOM.render(routing, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
